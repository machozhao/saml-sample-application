package com.ibm.lbs.saml.SnoopServlet;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;

/**
 * Un-used in container managed security
 */
public class LogoutServlet
        extends HttpServlet
{
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        HttpSession session = req.getSession(false);
        if (session != null) {
            session.invalidate();
        }

        String targetUrl = req.getParameter("targetUrl");
        if (targetUrl == null || targetUrl.trim().length() == 0) {
            // Redirect to app enrtance
            targetUrl = req.getContextPath();
        }
        res.sendRedirect(targetUrl);
    }

}
