package com.ibm.lbs.saml.SnoopServlet;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

public class MeServlet
        extends HttpServlet
{
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();

        out.println("<HTML><HEAD><TITLE>Me Servlet</TITLE></HEAD><BODY BGCOLOR=\"#FFEEFF\">");
        out.println("<h1>Me Servlet - Request/Client Information</h1>");
        out.println("<h2>Requested URL:</h2>");
        out.println("<TABLE Border=\"2\" WIDTH=\"65%\" BGCOLOR=\"#DDFFDD\">");
        out.println("<tr><td>" + escapeChar(HttpUtils.getRequestURL(req).toString()) + "</td></tr></table><BR><BR>");

        // Get username
        String username = req.getUserPrincipal().getName();
        out.println(String.format("<h2>Welcome <b>%s</b>.</h2>", username));
        out.println(String.format("<h4><A HREF=\"logout_manual.jsp\">Logout --- Manually</A><BR></h4>"));
        out.println(String.format("<h4><A HREF=\"logout_auto.jsp\">Logout --- Auto</A><BR></h4>"));

        out.println("<TABLE Border=\"2\" WIDTH=\"65%\" BGCOLOR=\"#DDFFDD\">");
        out.println("<tr><td>" + escapeChar(getServletConfig().getServletName()) + "</td></tr></table><BR><BR>");

        Enumeration vEnum = getServletConfig().getInitParameterNames();
        if ((vEnum != null) && (vEnum.hasMoreElements()))
        {
            boolean first = true;
            while (vEnum.hasMoreElements())
            {
                if (first)
                {
                    out.println("<h2>Servlet Initialization Parameters</h2>");
                    out.println("<TABLE Border=\"2\" WIDTH=\"65%\" BGCOLOR=\"#DDFFDD\">");
                    first = false;
                }
                String param = (String)vEnum.nextElement();
                out.println("<tr><td>" + escapeChar(param) + "</td><td>" + escapeChar(getInitParameter(param)) + "</td></tr>");
            }
            out.println("</table><BR><BR>");
        }
        vEnum = getServletConfig().getServletContext().getInitParameterNames();
        if ((vEnum != null) && (vEnum.hasMoreElements()))
        {
            boolean first = true;
            while (vEnum.hasMoreElements())
            {
                if (first)
                {
                    out.println("<h2>Servlet Context Initialization Parameters</h2>");
                    out.println("<TABLE Border=\"2\" WIDTH=\"65%\" BGCOLOR=\"#DDFFDD\">");
                    first = false;
                }
                String param = (String)vEnum.nextElement();
                out.println("<tr><td>" + escapeChar(param) + "</td><td>" + escapeChar(getServletConfig().getServletContext().getInitParameter(param)) + "</td></tr>");
            }
            out.println("</table><BR><BR>");
        }
        out.println("<h2>Request Information:</h2>");
        out.println("<TABLE Border=\"2\" WIDTH=\"65%\" BGCOLOR=\"#DDFFDD\">");
        print(out, "Request method", req.getMethod());
        print(out, "Request URI", req.getRequestURI());
        print(out, "Request protocol", req.getProtocol());
        print(out, "Servlet path", req.getServletPath());
        print(out, "Context Path", escapeChar(req.getContextPath()));
        if (req.getUserPrincipal() != null) {
            print(out, "User Principal", escapeChar(req.getUserPrincipal().getName()));
            print(out, "User Principal Detail", escapeChar(req.getUserPrincipal().getClass().getName()));
            print(out, "Auth Type", escapeChar(req.getAuthType()));
        } else {
            print(out, "User Principal", "none");
        }

        print(out, "Remote user", req.getRemoteUser());
        print(out, "Remote address", req.getRemoteAddr());
        print(out, "Remote host", req.getRemoteHost());
        print(out, "Remote port", req.getRemotePort());

        print(out, "Authorization scheme", req.getAuthType());
        out.println("</table><BR><BR>");

        Enumeration e = req.getHeaderNames();
        if (e.hasMoreElements())
        {
            out.println("<h2>Request headers:</h2>");
            out.println("<TABLE Border=\"2\" WIDTH=\"65%\" BGCOLOR=\"#DDFFDD\">");
            while (e.hasMoreElements())
            {
                String name = (String)e.nextElement();
                out.println("<tr><td>" + escapeChar(name) + "</td><td>" + escapeChar(req.getHeader(name)) + "</td></tr>");
            }
            out.println("</table><BR><BR>");
        }
        e = req.getParameterNames();
        if (e.hasMoreElements())
        {
            out.println("<h2>Servlet parameters (Single Value style):</h2>");
            out.println("<TABLE Border=\"2\" WIDTH=\"65%\" BGCOLOR=\"#DDFFDD\">");
            while (e.hasMoreElements())
            {
                String name = (String)e.nextElement();
                out.println("<tr><td>" + escapeChar(name) + "</td><td>" + escapeChar(req.getParameter(name)) + "</td></tr>");
            }
            out.println("</table><BR><BR>");
        }
        e = req.getParameterNames();
        if (e.hasMoreElements())
        {
            out.println("<h2>Servlet parameters (Multiple Value style):</h2>");
            out.println("<TABLE Border=\"2\" WIDTH=\"65%\" BGCOLOR=\"#DDFFDD\">");
            while (e.hasMoreElements())
            {
                String name = (String)e.nextElement();
                String[] vals = (String[])req.getParameterValues(name);
                if (vals != null)
                {
                    out.print("<tr><td>" + escapeChar(name) + "</td><td>");
                    out.print(escapeChar(vals[0]));
                    for (int i = 1; i < vals.length; i++) {
                        out.print(", " + escapeChar(vals[i]));
                    }
                    out.println("</td></tr>");
                }
            }
            out.println("</table><BR><BR>");
        }

        Cookie[] cookies = req.getCookies();
        if ((cookies != null) && (cookies.length > 0))
        {
            out.println("<H2>Client cookies</H2>");
            out.println("<TABLE Border=\"2\" WIDTH=\"65%\" BGCOLOR=\"#DDFFDD\">");
            for (int i = 0; i < cookies.length; i++) {
                out.println("<tr><td>" + escapeChar(cookies[i].getName()) + "</td><td>" + escapeChar(cookies[i].getValue()) + "</td></tr>");
            }
            out.println("</table><BR><BR>");
        }

        out.println("</body></html>");
    }

    private void print(PrintWriter out, String name, String value)
    {
        out.println("<tr><td>" + name + "</td><td>" + (value == null ? "&lt;none&gt;" : escapeChar(value)) + "</td></tr>");
    }

    private void print(PrintWriter out, String name, int value)
    {
        out.print("<tr><td>" + name + "</td><td>");
        if (value == -1) {
            out.print("&lt;none&gt;");
        } else {
            out.print(value);
        }
        out.println("</td></tr>");
    }

    private String escapeChar(String str)
    {
        char[] src = str.toCharArray();
        int len = src.length;
        for (int i = 0; i < src.length; i++) {
            switch (src[i])
            {
                case '<':
                    len += 3;
                    break;
                case '>':
                    len += 3;
                    break;
                case '&':
                    len += 4;
            }
        }
        char[] ret = new char[len];
        int j = 0;
        for (int i = 0; i < src.length; i++) {
            switch (src[i])
            {
                case '<':
                    ret[(j++)] = '&';
                    ret[(j++)] = 'l';
                    ret[(j++)] = 't';
                    ret[(j++)] = ';';
                    break;
                case '>':
                    ret[(j++)] = '&';
                    ret[(j++)] = 'g';
                    ret[(j++)] = 't';
                    ret[(j++)] = ';';
                    break;
                case '&':
                    ret[(j++)] = '&';
                    ret[(j++)] = 'a';
                    ret[(j++)] = 'm';
                    ret[(j++)] = 'p';
                    ret[(j++)] = ';';
                    break;
                default:
                    ret[(j++)] = src[i];
            }
        }
        return new String(ret);
    }
}
